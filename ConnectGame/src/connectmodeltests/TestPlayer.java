package connectmodeltests;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import connectmodel.PieceType;
import connectmodel.Player;

/**
 * Test cases for "ConnectGame"
 *
 * Tests functionality of the Player class in regards to validating correct names,
 * incrementing and getting score, getting correct piece type, getting correct name, 
 * and getting the correct number of wins.
 * 
 * @author hudson
 *
 */

public class TestPlayer 
{
	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testInvalidName1() 
	{
		Player player = new Player("Joe!", PieceType.YELLOW);
		assertTrue("Should have returned default name", player.getName() == Player.DEFAULT_NAME);
	}
	
	@Test
	public void testInvalidName2() 
	{
		Player player = new Player("Joe_", PieceType.YELLOW);
		assertTrue("Should have returned default name",player.getName() == Player.DEFAULT_NAME);
	}
	
	@Test
	public void testInvalidName3() 
	{
		Player player = new Player("????", PieceType.YELLOW);
		assertTrue("Should have returned default name",player.getName() == Player.DEFAULT_NAME);
	}
	
	@Test
	public void testInvalidName4() 
	{
		Player player = new Player("jo jo", PieceType.YELLOW);
		assertTrue("Should have returned default name",player.getName() == Player.DEFAULT_NAME);
	}
	
	@Test
	public void testValidName1() 
	{
		Player player = new Player("Joe", PieceType.YELLOW);
		assertTrue("Should have returned Joe",player.getName() == "Joe");
	}
	
	@Test
	public void testValidName2() 
	{
		Player player = new Player("joe", PieceType.YELLOW);
		assertTrue("Should have returned Joe",player.getName() == "Joe");
	}
	
	@Test
	public void testValidName3() 
	{
		Player player = new Player("JOE", PieceType.YELLOW);
		assertTrue("Should have returned Joe",player.getName() == "Joe");
	}
	
	@Test
	public void testValidName4() 
	{
		Player player = new Player("j0e", PieceType.YELLOW);
		assertTrue("Should have returned Joe",player.getName() == "Joe");
	}
	
	@Test
	public void testScoreIncrement() 
	{
		Player player = new Player("Joe", PieceType.YELLOW);
		assertTrue("Score should be zero on init", player.getScore() == 0);
		player.incrementScore();
		assertTrue("Score should be one after increment", player.getScore() == 1);
	}
	
	@Test
	public void testGetPieceType1() 
	{
		Player player = new Player("Joe", PieceType.YELLOW);
		assertTrue("Should have returned yellow piece type", player.getPieceType() == PieceType.YELLOW);
	}
	
	@Test
	public void testGetPieceType2() 
	{
		Player player = new Player("Joe", PieceType.RED);
		assertTrue("Should have returned red piece type", player.getPieceType() == PieceType.RED);
	}

}
