package connectmodeltests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import connectmodel.GameBoard;
import connectmodel.PieceType;

/**
 * Test cases for "ConnectGame"
 *
 * Tests functionality of the GameBoard class in regards to checking if win 
 * conditions work in the expected manner on a 6x7 board. 
 *   
 * @author hudson
 *
 */

public class TestWins6x7 {

	private PieceType[] myTypes = new PieceType[2];
	private GameBoard myGb;
	private Point point;
	
	@Before
	public void setUp() throws Exception {
		myTypes = new PieceType[]{PieceType.YELLOW, PieceType.GREEN};
		myGb = new GameBoard(6,7,2,myTypes);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * From bottom left
	 */

	@Test
	public void testDiagonalWin1() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(0, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(1, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 4", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 8", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 9", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 10", myGb.placePiece(3, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		point = new Point(0,0);
		assertTrue("Win should have started at (0,0)", myGb.getWinBegin().equals(point));
		point.move(3,3);
		assertTrue("Win should have ended at (3,3)", myGb.getWinEnd().equals(point));
		
	}
	
	/**
	 * From bottom right
	 */
	
	@Test
	public void testDiagonalWin2() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(4, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 4", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(2, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 8", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 9", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 10", myGb.placePiece(2, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		point = new Point(5,0);
		assertTrue("Win should have started at (5,0)", myGb.getWinBegin().equals(point));
		point.move(2,3);
		assertTrue("Win should have ended at (2,3)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * From top right
	 */
	
	@Test
	public void testDiagonalWin3() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(5, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(5, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 4", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(5, myTypes[1]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(5, myTypes[1]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(5, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		assertTrue("Couldn't place piece 8", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 9", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 10", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 11", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 12", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 13", myGb.placePiece(4, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		assertTrue("Couldn't place piece 14", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 15", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 16", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 17", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 18", myGb.placePiece(3, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		assertTrue("Couldn't place piece 19", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 20", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 21", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 22", myGb.placePiece(2, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		point = new Point(5,5);
		assertTrue("Win should have started at (5,6)", myGb.getWinBegin().equals(point));
		point.move(2,3);
		assertTrue("Win should have ended at (2,3)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * From top left
	 */
	
	@Test
	public void testDiagonalWin4() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(0, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(0, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(0, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 4", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(0, myTypes[1]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(0, myTypes[1]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(0, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		assertTrue("Couldn't place piece 8", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 9", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 10", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 11", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 12", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 13", myGb.placePiece(1, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		assertTrue("Couldn't place piece 14", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 15", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 16", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 17", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 18", myGb.placePiece(2, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		assertTrue("Couldn't place piece 19", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 20", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 21", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 22", myGb.placePiece(3, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		point = new Point(0,6);
		assertTrue("Win should have started at (0,6)", myGb.getWinBegin().equals(point));
		point.move(3,3);
		assertTrue("Win should have ended at (3,3)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * In center sloping down-left
	 */
	
	@Test
	public void testDiagonalWin5() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(1, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 4", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(1, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		assertTrue("Couldn't place piece 9", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 10", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 11", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 12", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 13", myGb.placePiece(2, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		assertTrue("Couldn't place piece 15", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 16", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 17", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 18", myGb.placePiece(3, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		assertTrue("Couldn't place piece 20", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 21", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 22", myGb.placePiece(4, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		
		point = new Point(1,5);
		assertTrue("Win should have started at (1,5)", myGb.getWinBegin().equals(point));
		point.move(4,2);
		assertTrue("Win should have ended at (4,2)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * Far left from bottom
	 */
	
	@Test
	public void testVerticalWin1() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 2", myGb.placePiece(0, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(0, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 4", myGb.placePiece(0, myTypes[1]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(0, myTypes[1]));
		assertTrue("Should have won", myGb.checkIfWin());
		
		point = new Point(0,0);
		assertTrue("Win should have started at (0,0)", myGb.getWinBegin().equals(point));
		point.move(0,3);
		assertTrue("Win should have ended at (0,3)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * Far right from bottom
	 */
	
	@Test
	public void testVerticalWin2() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(5, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		
		point = new Point(5,0);
		assertTrue("Win should have started at (5,0)", myGb.getWinBegin().equals(point));
		point.move(5,3);
		assertTrue("Win should have ended at (5,3)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * Far right to top
	 */
	
	@Test
	public void testVerticalWin3() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[1]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(5, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(5, myTypes[1]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(5, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		
		point = new Point(5,3);
		assertTrue("Win should have started at (5,3)", myGb.getWinBegin().equals(point));
		point.move(5,6);
		assertTrue("Win should have ended at (5,6)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * Far left to top
	 */
	
	@Test
	public void testVerticalWin4() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 1", myGb.placePiece(0, myTypes[1]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(0, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(0, myTypes[1]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(0, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		
		point = new Point(0,3);
		assertTrue("Win should have started at (0,3)", myGb.getWinBegin().equals(point));
		point.move(0,6);
		assertTrue("Win should have ended at (0,6)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * From middle to top
	 */
	
	@Test
	public void testVerticalWin5() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 1", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		
		point = new Point(3,3);
		assertTrue("Win should have started at (3,3)", myGb.getWinBegin().equals(point));
		point.move(3,6);
		assertTrue("Win should have ended at (3,6)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * Bottom left across bottom
	 */
	
	@Test
	public void testHorizontalWin1() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 1", myGb.placePiece(0, myTypes[1]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 3", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(3, myTypes[1]));
		assertTrue("Should have won", myGb.checkIfWin());
		
		point = new Point(0,0);
		assertTrue("Win should have started at (0,0)", myGb.getWinBegin().equals(point));
		point.move(3,0);
		assertTrue("Win should have ended at (0,0)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * Across the top in the middle
	 */
	
	@Test
	public void testHorizontalWin2() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 1", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(4, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 5", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 8", myGb.placePiece(4, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 9", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 10", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 11", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 12", myGb.placePiece(4, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 13", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 14", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 15", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 16", myGb.placePiece(4, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 17", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 18", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 19", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 20", myGb.placePiece(4, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 21", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 22", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 23", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 24", myGb.placePiece(4, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 25", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 26", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 27", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 28", myGb.placePiece(4, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		
		point = new Point(1,6);
		assertTrue("Win should have started at (1,6)", myGb.getWinBegin().equals(point));
		point.move(4,6);
		assertTrue("Win should have ended at (4,6)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * Center across the middle
	 */
	
	@Test
	public void testHorizontalWin3() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 9", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 10", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 11", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 12", myGb.placePiece(4, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 13", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 14", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 15", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 16", myGb.placePiece(4, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 17", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 18", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 19", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 20", myGb.placePiece(4, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 21", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 22", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 23", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 24", myGb.placePiece(4, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 25", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 26", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 27", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 28", myGb.placePiece(4, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
	}
	
	/**
	 * Bottom right across bottom
	 */
	
	@Test
	public void testHorizontalWin4() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[1]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(4, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 3", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(2, myTypes[1]));
		assertTrue("Should have won", myGb.checkIfWin());
		
		point = new Point(5,0);
		assertTrue("Win should have started at (5,0)", myGb.getWinBegin().equals(point));
		point.move(2,0);
		assertTrue("Win should have ended at (2,0)", myGb.getWinEnd().equals(point));
	}
	
	/**
	 * Center across bottom
	 */
	
	@Test
	public void testHorizontalWin5() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 1", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(3, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 3", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(1, myTypes[1]));
		assertTrue("Should have won", myGb.checkIfWin());
		
		point = new Point(4,0);
		assertTrue("Win should have started at (4,0)", myGb.getWinBegin().equals(point));
		point.move(1,0);
		assertTrue("Win should have ended at (1,0)", myGb.getWinEnd().equals(point));
	}

}
