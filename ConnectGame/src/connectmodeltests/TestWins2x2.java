package connectmodeltests;

import static org.junit.Assert.assertFalse;

/**
 * Test cases for "ConnectGame"
 *
 * Tests functionality of the GameBoard class in regards to checking if win 
 * conditions work in the expected manner on a 2x2 board. 
 *   
 * @author hudson
 *
 */

import static org.junit.Assert.assertTrue;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import connectmodel.GameBoard;
import connectmodel.PieceType;

public class TestWins2x2 
{
	
	private PieceType[] myTypes = new PieceType[2];
	private GameBoard myGb;
	private Point point;

	@Before
	public void setUp() throws Exception 
	{
		myTypes = new PieceType[]{PieceType.YELLOW, PieceType.GREEN};
		myGb = new GameBoard(2,2,2,myTypes);
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testDiagonalWin1() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece", myGb.placePiece(0, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(1, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		point = new Point(0,0);
		assertTrue("Win should have started on point (0,0)", myGb.getWinBegin().equals(point));
		point.move(1, 1);
		assertTrue("Win should have ended on point (1,1)", myGb.getWinBegin().equals(point));
	}
	
	@Test
	public void testDiagonalWin2() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece", myGb.placePiece(1, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(0, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(0, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		point = new Point(1,0);
		assertTrue("Win should have started on point (1,0)", myGb.getWinBegin().equals(point));
		point.move(0, 1);
		assertTrue("Win should have ended on point (0,1)", myGb.getWinBegin().equals(point));
	}
	
	@Test
	public void testHorizontalWin() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece", myGb.placePiece(0, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		point = new Point(0,0);
		assertTrue("Win should have started on point (0,0)", myGb.getWinBegin().equals(point));
		point.move(1, 0);
		assertTrue("Win should have ended on point (1,0)", myGb.getWinBegin().equals(point));
	}
	
	@Test
	public void testVerticalWin1() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece", myGb.placePiece(0, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(0, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		point = new Point(0,0);
		assertTrue("Win should have started at (0,0)", myGb.getWinBegin().equals(point));
		point.move(0,1);
		assertTrue("Win should have ended at (0,1)", myGb.getWinEnd().equals(point));
	}
	
	@Test
	public void testVerticalWin2() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece", myGb.placePiece(1, myTypes[1]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[1]));
		assertTrue("Should have won", myGb.checkIfWin());
		point = new Point(1,0);
		assertTrue("Win should have started at (1,0)", myGb.getWinBegin().equals(point));
		point.move(1,1);
		assertTrue("Win should have ended at (1,1)", myGb.getWinEnd().equals(point));
	}

}
