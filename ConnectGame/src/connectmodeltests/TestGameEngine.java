package connectmodeltests;

import java.util.Vector;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import connectmodel.GameBoard;
import connectmodel.GameEngine;
import connectmodel.PieceType;
import connectmodel.Player;

/**
 * Test cases for "ConnectGame"
 *
 * Tests functionality of the GameEngine class in regards to switching players,
 * returning the correct starting player, getting the next player up, returning the
 * players, and setting/ getting the game board.
 * 
 * @author hudson
 *
 */

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestGameEngine 

{
	private Vector<Player> myPlayers;
	private GameBoard myGameBoard;
	private Player myPlayer1;
	private Player myPlayer2;
	private PieceType[] myPieceTypes;
	private GameEngine myGameEngine;
	
	
	
	@Before
	public void setUp() throws Exception 
	{
		myPlayer1 = new Player("Bob", PieceType.GREEN);
		myPlayer2 = new Player("Larry", PieceType.RED);
		myPlayers = new Vector<Player>(2);
		myPlayers.add(myPlayer1);
		myPlayers.add(myPlayer2);
		
		myPieceTypes = new PieceType[2];
		myPieceTypes[0] = myPlayers.get(0).getPieceType();
		myPieceTypes[1] = myPlayers.get(1).getPieceType();
		
		myGameBoard = new GameBoard(6,7,4, myPieceTypes);
		
		myGameEngine = new GameEngine(myPlayers, myGameBoard);
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testStartingAndSwitchingPlayer1()
	{
		assertTrue("Should have started game", myGameEngine.startGame());
		assertTrue("Should be able to set starting player", myGameEngine.selectStartingPlayer(myPlayers.get(0)));
		assertTrue("Should have gotten Player1", myGameEngine.getPlayerUp() == myPlayers.get(0));
		assertTrue("Should have switched to Player2", myGameEngine.switchPlayerUp() == myPlayers.get(1));
		assertTrue("Should have switched to Player1", myGameEngine.switchPlayerUp() == myPlayers.get(0));
		assertTrue("Should have returned Player1 as starting player", myGameEngine.getStartingPlayer() == myPlayers.get(0));
		assertTrue("Should have returned the Player vector", myGameEngine.getPlayers() == myPlayers);
	}
	
	@Test
	public void testStartingAndSwitchingPlayer2()
	{
		assertTrue("Should have started game", myGameEngine.startGame());
		assertTrue("Should be able to set starting player", myGameEngine.selectStartingPlayer(myPlayers.get(1)));
		assertTrue("Should have gotten Player1", myGameEngine.getPlayerUp() == myPlayers.get(1));
		assertTrue("Should have switched to Player2", myGameEngine.switchPlayerUp() == myPlayers.get(0));
		assertTrue("Should have switched to Player1", myGameEngine.switchPlayerUp() == myPlayers.get(1));
		assertTrue("Should have returned Player1 as starting player", myGameEngine.getStartingPlayer() == myPlayers.get(1));
		assertTrue("Should have returned the Player vector", myGameEngine.getPlayers() == myPlayers);
	}
	
	@Test
	public void testGameBoardInteractions1()
	{
		assertTrue("Should have started game", myGameEngine.startGame());
		assertTrue("Should be able to set starting player", myGameEngine.selectStartingPlayer(myPlayers.get(0)));
		assertTrue("Should have returned empty", myGameEngine.getGameBoard() == null);
		myGameEngine.setGameBoard(myGameBoard);
		assertTrue("Should have returned empty", myGameEngine.getGameBoard() == null);
	}
	
	@Test
	public void testGameBoardInteractions2()
	{
		assertTrue("Should have started game", myGameEngine.startGame());
		assertTrue("Should be able to set starting player", myGameEngine.selectStartingPlayer(myPlayers.get(0)));
		assertTrue("Should have returned empty", myGameEngine.getGameBoard() == null);
		myGameEngine.setGameBoard(myGameBoard);
		assertTrue("Should have returned empty", myGameEngine.getGameBoard() == null);
		assertTrue("Should have been able to place piece", myGameEngine.placePiece(0));
		myGameEngine.setGameBoard(myGameBoard);
		assertTrue("Should have returned the GameBoard", myGameEngine.getGameBoard() == myGameBoard);
	}
}
