package connectmodeltests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import connectmodel.GameBoard;
import connectmodel.PieceType;

/**
 * Test cases for "ConnectGame"
 *
 * Tests functionality of the GameBoard class in regards to creating a set size myBoard 
 * and checking if the GameBoard class restricts piece placement to within the created 
 * myBoard.
 *   
 * @author hudson
 *
 */

public class TestValidPlacements 
{
	
	private GameBoard myBoard;
	private PieceType[] myTypes = new PieceType[2];

	@Before
	public void setUp() throws Exception 
	{
		myTypes = new PieceType[]{PieceType.BLACK, PieceType.RED};
		myBoard = new GameBoard(3,3,3,myTypes);
	}

	@After
	public void tearDown() throws Exception 
	{
	}
	
	/**
	 * Tests to see if pieces can be placed in columns less than zero
	 */

	@Test
	public void columnLessThanZero1() 
	{
		assertTrue("Should be able to place", myBoard.placePiece(1, myTypes[0]));
		assertTrue("Should be able to place", myBoard.placePiece(0, myTypes[0]));
		assertFalse("Should not be able to place", myBoard.placePiece(-1, myTypes[0]));
		assertFalse("Should not count as win", myBoard.checkIfWin());
	}
	
	@Test
	public void columnLessThanZero2() 
	{
		assertTrue("Should be able to place", myBoard.placePiece(0, myTypes[0]));
		assertFalse("Shouldn't be able to place", myBoard.placePiece(-1, myTypes[0]));
		assertFalse("Should not be able to place", myBoard.placePiece(-2, myTypes[0]));
		assertFalse("Should not count as win", myBoard.checkIfWin());
	}
	
	@Test
	public void columnLessThanZero3() 
	{
		assertFalse("Should be able to place", myBoard.placePiece(-10, myTypes[0]));
		assertFalse("Shouldn't be able to place", myBoard.placePiece(-11, myTypes[0]));
		assertFalse("Should not be able to place", myBoard.placePiece(-12, myTypes[0]));
		assertFalse("Should not count as win", myBoard.checkIfWin());
	}
	
	/**
	 * Tests to see if piece can be placed in columns greater than max
	 */
	
	@Test
	public void columnGreaterThanMax1() 
	{
		assertTrue("Should be able to place", myBoard.placePiece(1, myTypes[0]));
		assertTrue("Should be able to place", myBoard.placePiece(2, myTypes[0]));
		assertFalse("Should not be able to place", myBoard.placePiece(3, myTypes[0]));
		assertFalse("Should not count as win", myBoard.checkIfWin());
	}

	@Test
	public void columnGreaterThanMax2() 
	{
		assertFalse("Should not be able to place",myBoard.placePiece(10, myTypes[0]));
	}
	
	/**
	 * Tests to see if pieces can be placed above the maximum height of the board
	 */
	
	@Test
	public void rowAboveHeight1() 
	{
		myBoard.placePiece(0, myTypes[0]);
		myBoard.placePiece(0, myTypes[1]);
		myBoard.placePiece(0, myTypes[0]);
		assertFalse("Should not be able to place", myBoard.placePiece(0, myTypes[1]));
	}

	@Test
	public void rowAboveHeight2() 
	{
		myBoard.placePiece(0, myTypes[1]);
		myBoard.placePiece(0, myTypes[0]);
		myBoard.placePiece(0, myTypes[1]);
		myBoard.placePiece(0, myTypes[0]);
		myBoard.placePiece(0, myTypes[0]);
		myBoard.placePiece(0, myTypes[0]);
		assertFalse("Should not be able to place", myBoard.placePiece(0, myTypes[1]));
		assertFalse("Should not be able to win out of bounds", myBoard.checkIfWin());
	}
	
	@Test
	public void rowAboveHeight3() 
	{
		myBoard.placePiece(2, myTypes[0]);
		myBoard.placePiece(2, myTypes[1]);
		myBoard.placePiece(2, myTypes[0]);
		assertFalse("Should not be able to place", myBoard.placePiece(2, myTypes[1]));
	}
	
	@Test
	public void rowAboveHeight4() 
	{
		myBoard.placePiece(2, myTypes[1]);
		myBoard.placePiece(2, myTypes[0]);
		myBoard.placePiece(2, myTypes[1]);
		myBoard.placePiece(2, myTypes[0]);
		myBoard.placePiece(2, myTypes[0]);
		myBoard.placePiece(2, myTypes[0]);
		assertFalse("Should not be able to place", myBoard.placePiece(2, myTypes[1]));
		assertFalse("Should not be able to win out of bounds", myBoard.checkIfWin());
	}

}
