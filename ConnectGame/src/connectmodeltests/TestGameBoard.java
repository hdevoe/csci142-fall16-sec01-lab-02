package connectmodeltests;

import static org.junit.Assert.*;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import connectmodel.GameBoard;
import connectmodel.PieceType;

/**
 * Test cases for "ConnectGame"
 *
 * Tests functionality of the GameBoard class in regards to functions not checked
 * in the testValidPlacement and testValidWins test cases.
 *   
 * @author hudson
 *
 */

import org.junit.Test;

public class TestGameBoard 
{
	
	private PieceType[] myTypes = new PieceType[2];
	private GameBoard myGb;
	private Point point;
	
	@Before
	public void setUp() throws Exception 
	{
		myTypes = new PieceType[]{PieceType.YELLOW, PieceType.GREEN};
		myGb = new GameBoard(6,7,2,myTypes);
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public void testCheckAllNull() 
	{
		assertTrue("GameBoard should have returned empty", myGb.checkAllNull());
		assertTrue("Should have been able to place piece", myGb.placePiece(1,PieceType.YELLOW));
		assertFalse("GameBoard should not have returned empty", myGb.checkAllNull());
	}
	
	@Test
	public void testCheckAllNull2() 
	{
		assertTrue("GameBoard should have returned empty", myGb.checkAllNull());
		assertTrue("Should have been able to place piece", myGb.placePiece(1,PieceType.YELLOW));
		assertFalse("GameBoard should not have returned empty", myGb.checkAllNull());
		myGb.resetBoard();
		assertTrue("GameBoard should have returned empty", myGb.checkAllNull());
	}
	
	@Test
	public void testGetTypes() 
	{
		assertTrue("Should have returned correct piece types", myGb.getTypes().equals(myTypes));
	}
	
	@Test
	public void testGetLastPoint1() 
	{
		assertTrue("Should have returned null", myGb.getLastPoint() == null);
	}
	
	@Test
	public void testGetLastPoint2() 
	{
		assertTrue("Should have returned null", myGb.getLastPoint() == null);
		assertTrue("Should have been able to place piece", myGb.placePiece(0,myTypes[0]));
		point = new Point(0,0);
		assertTrue("Should have returned (0,0)", myGb.getLastPoint() == point);
	}
	
	@Test
	public void testGetLastPoint3() 
	{
		assertTrue("Should have returned null", myGb.getLastPoint() == null);
		assertTrue("Should have been able to place piece", myGb.placePiece(3,myTypes[0]));
		point = new Point(3,0);
		assertTrue("Should have returned (3,0)", myGb.getLastPoint() == point);
		assertTrue("Should have been able to place piece", myGb.placePiece(3,myTypes[0]));
		assertTrue("Should have been able to place piece", myGb.placePiece(3,myTypes[0]));
		assertTrue("Should have been able to place piece", myGb.placePiece(3,myTypes[0]));
		point.move(3,3);
		assertTrue("Should have returned (3,3)", myGb.getLastPoint() == point);
	}
	
	@Test
	public void testGetPieceOnBoard1() 
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(0, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(1, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 4", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 8", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 9", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 10", myGb.placePiece(3, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		point = new Point(2,2);
		assertTrue("Should have returned yellow", myGb.getPieceOnBoard(point) == myTypes[0]);
		point = new Point(4,4);
		assertTrue("Should have returned null", myGb.getPieceOnBoard(point) == null);
	}
	
	@Test
	public void testGetPieceOnBoard2() 
	{

			myGb.resetBoard();
			assertTrue("Couldn't place piece 1", myGb.placePiece(0, myTypes[0]));
			assertFalse("Shouldn't have won", myGb.checkIfWin());
			assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[1]));
			assertTrue("Couldn't place piece 3", myGb.placePiece(1, myTypes[0]));
			assertFalse("Shouldn't have won", myGb.checkIfWin());
			assertTrue("Couldn't place piece 4", myGb.placePiece(2, myTypes[1]));
			assertTrue("Couldn't place piece 5", myGb.placePiece(2, myTypes[1]));
			assertTrue("Couldn't place piece 6", myGb.placePiece(2, myTypes[0]));
			assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
			assertFalse("Shouldn't have won", myGb.checkIfWin());
			assertTrue("Couldn't place piece 8", myGb.placePiece(3, myTypes[1]));
			assertTrue("Couldn't place piece 9", myGb.placePiece(3, myTypes[1]));
			assertTrue("Couldn't place piece 10", myGb.placePiece(3, myTypes[0]));
			
			point = new Point(0,0);
			assertTrue("Should have returned yellow", myGb.getPieceOnBoard(point) == myTypes[0]);
			point = new Point(3,6);
			assertTrue("Should have returned null", myGb.getPieceOnBoard(point) == null);

	}
	
	@Test
	public void testGetPieceOnBoard3() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(5, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		
		point = new Point(5,0);
		assertTrue("Should have returned yellow", myGb.getPieceOnBoard(point) == myTypes[0]);
		point = new Point(5,6);
		assertTrue("Should have returned null", myGb.getPieceOnBoard(point) == null);
	}
	
	@Test
	public void testIsColumnFull1()
	{
		myGb.resetBoard();
		for (int i = 0; i < 6; i++) {
			assertFalse("Columns should all be empty", myGb.isColumnFull(i));
		}
	}
	
	@Test
	public void testIsColumnFull2()
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(5, myTypes[0]));
		assertFalse("Column should be empty", myGb.isColumnFull(3));
		assertFalse("Column should be empty", myGb.isColumnFull(0));
		assertTrue("Column should be full", myGb.isColumnFull(5));
	}
	
	@Test
	public void testIsColumnFull3()
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(0, myTypes[0]));
		assertFalse("Column should be empty", myGb.isColumnFull(3));
		assertFalse("Column should be empty", myGb.isColumnFull(5));
		assertTrue("Column should be full", myGb.isColumnFull(0));
	}
	
	@Test
	public void testIsColumnFull4()
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
		assertFalse("Column should be empty", myGb.isColumnFull(0));
		assertFalse("Column should be empty", myGb.isColumnFull(5));
		assertTrue("Column should be full", myGb.isColumnFull(3));
	}
	
	@Test
	public void testIsBoardFull()
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(0, myTypes[0]));
		assertFalse("Board shouldn't be full", myGb.isBoardFull());
		assertTrue("Couldn't place piece 1", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(1, myTypes[0]));
		assertFalse("Board shouldn't be full", myGb.isBoardFull());
		assertTrue("Couldn't place piece 1", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(2, myTypes[0]));
		assertFalse("Board shouldn't be full", myGb.isBoardFull());
		assertTrue("Couldn't place piece 1", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
		assertFalse("Board shouldn't be full", myGb.isBoardFull());
		assertTrue("Couldn't place piece 1", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(4, myTypes[0]));
		assertFalse("Board shouldn't be full", myGb.isBoardFull());
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(5, myTypes[0]));
		assertTrue("Board should be full", myGb.isBoardFull());
	}
	
	@Test
	public void testGetBestMove1()
	{
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(5, myTypes[0]));
		assertTrue("Should have connected four", myGb.findBestMoveColumn(myTypes[0]) == 5);
	}
	
	@Test
	public void testGetBestMove2()
	{
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(3, myTypes[0]));
		assertTrue("Should have connected four", myGb.findBestMoveColumn(myTypes[0]) == 2);
	}
	
	@Test
	public void testGetBestMove3()
	{
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(3, myTypes[0]));
		assertTrue("Should have blocked win", myGb.findBestMoveColumn(myTypes[1]) == 2);
	}
	
	@Test
	public void testGetBestMove4()
	{
		assertTrue("Couldn't place piece 1", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(5, myTypes[0]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(5, myTypes[0]));
		assertTrue("Should have blocked win", myGb.findBestMoveColumn(myTypes[1]) == 5);
	}
	
	@Test
	public void testGetBestMove5()
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 8", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 9", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 10", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 11", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 12", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 13", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 14", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 15", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 16", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 17", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 18", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 19", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 20", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 21", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 22", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 23", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 24", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 25", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 26", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 27", myGb.placePiece(3, myTypes[0]));
		assertTrue("Should have connected four", myGb.findBestMoveColumn(myTypes[0]) == 4);
	}
	
	@Test
	public void testGetBestMove6()
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 8", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 9", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 10", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 11", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 12", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 13", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 14", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 15", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 16", myGb.placePiece(4, myTypes[0]));
		assertTrue("Couldn't place piece 17", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 18", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 19", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 20", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 21", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 22", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 23", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 24", myGb.placePiece(4, myTypes[1]));
		assertTrue("Couldn't place piece 25", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 26", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 27", myGb.placePiece(3, myTypes[0]));
		assertTrue("Should have blocked win", myGb.findBestMoveColumn(myTypes[1]) == 4);
	}
	
	@Test
	public void testGetBestMove7()
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 8", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 9", myGb.placePiece(3, myTypes[1]));
		assertTrue("Should have connected four", myGb.findBestMoveColumn(myTypes[0]) == 3);
	}
	
	@Test
	public void testGetBestMove8()
	{
		myGb.resetBoard();
		assertTrue("Couldn't place piece 1", myGb.placePiece(0, myTypes[0]));
		assertTrue("Couldn't place piece 2", myGb.placePiece(1, myTypes[1]));
		assertTrue("Couldn't place piece 3", myGb.placePiece(1, myTypes[0]));
		assertTrue("Couldn't place piece 4", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 5", myGb.placePiece(2, myTypes[1]));
		assertTrue("Couldn't place piece 6", myGb.placePiece(2, myTypes[0]));
		assertTrue("Couldn't place piece 7", myGb.placePiece(3, myTypes[0]));
		assertTrue("Couldn't place piece 8", myGb.placePiece(3, myTypes[1]));
		assertTrue("Couldn't place piece 9", myGb.placePiece(3, myTypes[1]));
		assertTrue("Should have blocked win", myGb.findBestMoveColumn(myTypes[1]) == 3);
	}
}
