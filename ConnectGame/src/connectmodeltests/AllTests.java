package connectmodeltests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Test suite constructed of all test cases
 * @author hudson
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ 
	TestGameEngine.class, 
	TestPlayer.class, 
	TestValidPlacements.class, 
	TestWins1x1.class,
	TestWins2x2.class,
	TestWins6x7.class,
	TestGameBoard.class
	})
public class AllTests {

}
