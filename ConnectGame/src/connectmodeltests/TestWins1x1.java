package connectmodeltests;

import static org.junit.Assert.assertFalse;

/**
 * Test cases for "ConnectGame"
 *
 * Tests functionality of the GameBoard class in regards to checking if win 
 * conditions work in the expected manner on a 1x1 board. 
 *   
 * @author hudson
 *
 */

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import connectmodel.GameBoard;
import connectmodel.PieceType;

public class TestWins1x1 {

	private PieceType[] myTypes = new PieceType[2];
	private GameBoard myGb;
	
	@Before
	public void setUp() throws Exception {
		myTypes = new PieceType[]{PieceType.YELLOW, PieceType.GREEN};
		myGb = new GameBoard(1,1,1,myTypes);
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Tests to see if wins work on a 1x1 board
	 */
	@Test
	public void testWin1() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece", myGb.placePiece(0, myTypes[0]));
		assertTrue("Should have won", myGb.checkIfWin());
		
	}
	
	@Test
	public void testWin2() 
	{
		myGb.resetBoard();
		assertFalse("Shouldn't have won", myGb.checkIfWin());
		assertTrue("Couldn't place piece", myGb.placePiece(0, myTypes[1]));
		assertTrue("Should have won", myGb.checkIfWin());
		
	}

}
